import React from 'react';

class Connect extends React.Component{
    render(){
        return(
            <div className = "connectContainer">

                <div className = "contactContent">
                    <div className = "contentZero">
                        <div className = "connectContent clearfix">
                            <div className = "imgConnectContainer">
                                <img src = "https://image.flaticon.com/icons/svg/1034/1034138.svg"/>
                            </div>
                            <div className = "infoContainer">jhamilapadilla@gmail.com</div>
                        </div>    

                        <div className = "connectContent clearfix">
                            <div className = "imgConnectContainer">
                                <img src = "https://image.flaticon.com/icons/svg/481/481300.svg"/>
                            </div>
                            <div className = "infoContainer">+ 63 928 825 1401</div>
                        </div>   
                        
                        <div className = "connectContent clearfix">
                        <div className = "imgConnectContainer">
                            <img src = "https://image.flaticon.com/icons/svg/944/944602.svg"/>
                        </div>
                        <div className = "infoContainer">Makati City, Philippines</div>
                    </div>               
                    </div>
                    <div className = "contentTwo clearfix">
                        <div className = "imgConnectContainer">
                            <a href = "https://www.linkedin.com/in/jhamila-padilla-9a3735119/">
                                <img src = "https://image.flaticon.com/icons/svg/408/408805.svg"/>
                            </a>
                        </div>
                        <div className = "imgConnectContainer">
                            <a href = "https://gitlab.com/jhamphee">
                                <img src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAKbElEQVR4Xu3bdZBtRxEG8C+4u1NAgru7h1C4u7u7u5Pg7lq4uztVuLtr4e7uUL9lZmv25Ojeu0uo9/qfl71nznTPN+19sk/2cNpnDz9/9gKwVwOWI3DFJPdPcqgkD0ny+uVbrPUN8jyg7PjQpfIsNYFTJ/lskiMWhn9OcvYkX13rkeZv1ifPWZJ8Y+4WSwE4KMl9Ops/Msm95jJc87qHJbl3Z08y3m8unyUAWPuVJKdJ8qkk/0xy7iTfTOIm/j2X6ZrWkYfm4f2xZMOhk8dvp5vLYwkAZ0jyxbIxLQCA20fU7vNzma5p3RmTfKHsdY8CQJWHrF+ew2cJABzNg8umtOBfja1xhg+cw3CNa/B7UNnvFMUpV9snK4c4SUsA4Pzc9JeSQB99LsmZi2acaZLbehdU3uQ6W0ee9rdRrnMBgDBbR+1td7Xi6+s94+Bup2y0T0g+sKxs5SHzt6fkmQsAG+uz965fePgUwzU9v2eSR5S9Tl+csz9becj86Cl+cwHgZfs8fjcynHOK4ZqefzzJucrBAVCplYfM553iNweAkyT5XtnoUUmg31Ibi/dN8t0ppis+P2nDg+ozgSF5yP6DMX5zALhDkieWTc6TBPotnSPJJ8sPd0ny+BUPOPX6nRoenB+HNyQP2Z+8KgDvS3LhJN9PcrKehAeInI3b/2CSC02dYMXnH0hywcKTM+wmYK08ZL/oKgAcP8mPS5JBC6DfR49N4vYJc6IkP1nxkEOvnyDJj4o8HBxHNyaPXOWESX42tOGUCdwiyTPLyxdJ8v6Bjc6f5EPl2a2TPGOHALD308reHBxH10cXKNromTM8e7sAvCPJJQqCblb620dKYyZizbvKOzuBwbuTHFAcG3N0w0PycH5u3xkutR0AjlkOfpiiBbeaONFTktw2yT+SMJ1frRmBYyf5aZJDJ3lSkjtO7P/UJLcp8hwvya/71o+ZwA2TPL+8RAvc7Bjtn+S9ZcGNkrxgzQDcJMlzZ5hjZXuxJO8pfzjLC5cC8IYkVyjIudG/TxyIpnCYx0ny5iSXXzMAb0lymRnmWNmShzOmOc5ypSUAHDXJz5McvtykG51DnM3NkvwtCbX77ZyXZqw5epHnsEmeleSWM96xhMbQnL8kOW6SP3TfGzIBDKonF+PfNJPhpUuDwvK3LmlNTex/2iSXLGs0Zd45Ux4ac6qytjcaDAGgqqsvzuR1iF/GKTOHLdQHwJGS/LFZJfQdTHVGjktdK/Ebf1oRmiMUU6zbLDGro5SoUd/1d3u23sHIlZO8tiP0WBbYPd9Vkrym/GivVdvmHDEnhuz9upmAklkt0NLVk7y6/aFPA16S5Dolfn6t1NjeuWaSV85gfuTisLTOX5TkBjPeGVsiFAtjWvAizByNIuvLy6b6mFp4HOgrklxrDADqJm8WBRxW6fvpJJIiZqAGnzMDoEFu/zclKRIVtkOElvzgb8+rztiEw/xEEuou+TG30KhxcOovGgBzg7oaIHa/sTy7RpJXJblsiet+1g9UEm+xox6hrldu3yPee67X7m518SYBsyftHCOHVh/UJgnZRaOrlbN4d4tZdgGQvVFZCInj1flpPNy3cCbE9SfmAMcoZjA3jR46lMJHAcSZkodGDZGzvLiYrzVts6Q1S/ID82AacLii/rx4V93k34oKhQiSYz994jbeXm6fCp94pJAa2kaB9cMkSmB7yTHGiEzyf6RoUgC1xRvHzIn+roD5VwtbDfDC28oGnODLOtzcAH/gMGxaU4KtDVFbSmuSaJYsobbEHi1pS7/S/nwG0HSKZLItXTvJS8sPl0sitd4CwHOS3DQJZBwWUl0ilC4L1dYn5GB+OXAqe8jFgaxNpmGyhGqTZaqpIblxMXqFKlHdq4/0MDpaAYWmiyw37gLwndLykvaKvUPU9uRoDDSH6vLaTtMo3W/B/HBuW4uZuMla75Ot9i/75FekcYx6FwDbogEyLCgpG8XdISKceCqpQGNjKDX7E8o6zVM3NYeocF1rD/V/H7WDEGFbqBsb0lYnT7s3MtbWB7jNiqQhiHbzUAkMKPZfp8JCXV+/oG1hLxlbm+vVEbc93FiX9Cg4R2eQsMlRfj8AFN9gz9rS33SqLQBiJ0ci6UAfTsJx1JlAd2/zQTFX7fCL4g/6BAWUgYkqrh1ijGmCfMNa7xrIdEm//zOluJEZWuOdPtI649DPVx4KpXqGG9Pjbh4gZaTehqBIJqUXUJOjLoM24floEo3TbtbnAwbDE9SOsYYAkMkBC3m3jsDqek5Mc1ZChsYSJJ/PPK+5VKBJkze/IOmrBaTDjysJSGXKjqlPX0orH6j9wr5eHVBr+iyZqmAMAdAC5t3uwBWP25eX8Rb/u6SRw4zbvqEByd1LlNtcP9YT5OSERvaOTH84mW91uGHGdOpc0Bpa1JKCxOCSY+MMxwgfa7zTHbnbu+YnzEN+sZHQNGRYgr8Qjai88N6tcDceTs0FTl42q4fjPW/eUxWaCo0VTUbqdYYnHAq5fcRe67PuRxc+e3Foaa3mhgN255DUW1tOMYeM8fw2xG8SAJuwOXZ450Zi7TKJzWZVVRqWG9lVcTBstNYSZy1Oy7Ox+WGbY3jHRxBIkeMwQBDmxPKatXqu9GamMsZKjyn1y2glOqUB7S1JjmRQNUr4Jgi6bXnchi9pJwdFYHyYjtsfmx9ybtRaH7LO/byrgBGREB71u0B/A4XKV3ORmcpj6mUMKNt/f14CgPViMhuUEiNlsWpN4wMpmsRYZSwyKKmjLDdy15H5YTuHtJbDqnsYuiC5hqKoFjkOan+hGAEXUKMj8RaRpQB4V1LBPttvA2nG7ZqGg3CjaJJIKZqob1vciBp15ljlaYsna+XzzMg0GE+HYveKHCah8qvdJlomuvhoSj0wm7YDQN1c9ufmdVgQU9BE8emapIM6t0WTnKLO6zRIapu77lfLZ9Nfic6ximP1322R46Msaa8QiXSwmNrU5KoXlFUAsKFhqCaEsRgygNCIFD79W+sAh+O4xOK+eV07h3Sz4jcbriD527vmFfYUepFR3HVXGcevCgAh2L0Ex3d7qjOkIUlYIYlWIM+N0DUrEPWtvkOHqc7uNF04wvoNoNuu/YBagKk+7afXNzSx7r3x7o/rAKDu6UsMnt9IGvH6EhC2Tl3ZqdLZoam3drn+HNLqNrsT34GhbCUbs6IxNEpOgpiIho1Se2VaJwCE0QRxk1V1xWC5uEPx1EIUtXWTzEWbGymmpOBumwZocihyAMrTc4JI7Pd3t9uzbSDWDQBBmIEQpvxlHl2S2yujkW4tqsMKZW11bu171FyNoEs01HzZFgg7AUAVRCjjC3jxLgmPbrX26Kh0/a27VjmuBuhrc23r0O1LOwkAPmydCQy12GohU71690D8BD+y7q9NNvnsNAAY4SEk+qqr2vLUzdGGu5XQt6P/H8JuAFAPq6KUs1dvPgSCOkCNUT++nAJrpee7CQBBNSLlBjWed4WvMX/JCPz/CoBqEgqoOsWpBxDvldk7qvJdtHZbA1r++nV11i8JGuo7rnTDUy//LwGYkm1Xnu8FYFdgPgQz2eM14D9VmzpfrqVFfgAAAABJRU5ErkJggg=="/>
                            </a>
                        </div>
                        <div className = "imgConnectContainer">
                            <a href = "https://www.facebook.com/jhamila.padilla">
                                <img src = "https://image.flaticon.com/icons/svg/725/725350.svg"/>
                            </a>
                        </div>
                        <div className = "imgConnectContainer">
                            <a href = "https://www.instagram.com/jhamphee/">
                                <img src = "https://image.flaticon.com/icons/svg/1077/1077042.svg"/>
                            </a>
                        </div>
                    </div>
                </div>

                <form>
                    <div>
                        <input type = "text" placeholder = "Name"/>
                    </div>
                    <div>
                        <input type = "email" placeholder = "Email"/>
                    </div>
                    <div>
                        <input type = "phone" placeholder = "Phone"/>
                    </div>
                    <div>
                        <input id = "message" type = "text" placeholder = "Hi! How can I help?"/>
                    </div>
                    <div>
                        <button type = "button">SUBMIT</button>
                    </div>

                </form>

            </div>
        )
    }
}

export default Connect;