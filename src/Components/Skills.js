import React from 'react';

import Slide from 'react-reveal/Slide';


class Skills extends React.Component{
    render(){
        return(
            <Slide top cascade>
            <div className = "skillsContainer clearfix">
                <div className = "skillsContent">
                    <strong>FULL STACK WEB DEVELOPMENT</strong>
                    <div className = "skills">HTML5</div>
                    <div className = "skills">CSS3</div>
                    <div className = "skills">Bootstrap4</div>
                    <div className = "skills">JavaScript</div>
                    <div className = "skills">MongoDB</div>
                    <div className = "skills">Express</div>
                    <div className = "skills">Node.js</div>
                    <div className = "skills">ReactJS</div>
                    <div className = "skills">React-Redux</div>
                </div>

                <div className = "skillsContent">
                    <strong>CUSTOMER SUPPORT</strong>
                    <div className = "skills">Billing</div>
                    <div className = "skills">Telecommunications</div>
                    <div className = "skills">Financial</div>
                </div>

                <div className = "skillsContent">
                    <strong>SALES</strong>
                    <div className = "skills">Telecommunications</div>
                </div>

                <div className = "skillsContent">
                    <strong>LEADS GENERATION</strong>
                    <div className = "skills">MedSpa</div>
                    <div className = "skills">Construction</div>
                    <div className = "skills">Pet Services</div>
                    
                </div>
            </div>
            </Slide>
        )
    }
}

export default Skills;