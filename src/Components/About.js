import React from 'react';
import {BrowserRouter, Route, Link} from 'react-router-dom';

// import Fade from 'react-reveal/Fade';

class Container extends React.Component{
    constructor(props) {
        super(props);
        this.state = { show: false };
        this.clickHandler = this.clickHandler.bind(this);
      }

    render(){
        return(
            <div className = "aboutContainer clearfix">

            <div className = "aboutMeContainer">
                Hello there! I'm Jhamila and I'm a full stack web developer from the Philippines.
                I enjoy cooking and baking whenever I have the time and the energy, I also enjoy 
                travelling outdoors solo, sometimes with friends. Oh, I hate spiders too. 
            </div>
    
            <div className = "objectiveContainer">
            I am a detail-oriented Social Work major currently attending The
            Philippine Women's University, with 2.5+ years of work
            experience in customer service and leads generation. Aiming
            to leverage a proven knowledge of web platform
            development, database development, and web design skills
            to successfully fill the Junior Web Developer role at your
            company. Frequently praised as efficient by my peers in
            getting the work done in a timely manner with exceptional
            results, I can be relied upon to help your company achieve its
            goals
            </div>

            <div className = "clickTab" onClick = {this.clickHandler}>MORE? MORE!</div>
            {/* <div className = "containerOne clearfix" > 

                <div className = "workHighlightsContainer">
                    <div className = "highlighted">
                        <strong> WORK EXPERIENCE </strong> 
                    </div>
                    <div className = "workContainer">
                        <div className = "bulletedContainer">
                            <strong>ACCOUNT ASSOCIATE</strong>
                            <div>VXI Global Solutions| Feb 2019 to Sept 2019</div>
                            <div>
                                Customer support for accounts and transactions under Paypal for U.S. and
                                Canada. Handled Low and High Service Level accounts and able to close
                                70 tickets within a day.
                            </div>
                        </div>

                        <div className = "bulletedContainer">
                            <strong>LEAD SOURCING SPECIALIST</strong>
                            <div>Outsourcing2PH| Aug 2017 to Oct 2018</div>
                            <div>
                            I worked as a freelancer sourcing for leads, prospects, and potential
                            customers for the company and their clientele, specifically sourcing med
                            spas, constructions, and pet services accounts.
                            </div>
                        </div>

                        
                        <div className = "bulletedContainer">
                            <strong>CUSTOMER SERVICE REPRESENTATIVE – INBOUND LEVEL 3</strong>
                            <div>IBEX Global Solutions Inc.| May 2016 to Dec 2016</div>
                            <div>
                            Customer support, handling billing for AT&T Uverse and DirectTV sales
                            for MoKAT region. Also handled AT&T ISM for billing, tier-1 technical
                            support. and sales for both Uverse and DirectTv
                            </div>
                        </div>
                    </div>   
                    <div className = "defContainer">
                        {}
                    </div>
                </div>
                
                <div className = "educHighlightsContainer">
                    <div className = "highlighted">
                        <strong>SCHOOLS ATTENDED</strong>
                    </div>

                    <div className = "bulletedContainer">
                        <div>UPLIFT CODE CAMP</div>
                        <div>Full-Stack Web Development Certificate</div>
                        <div>Dec 2019 - March 2020</div>
                    </div>

                    <div className = "bulletedContainer">
                        <div>THE PHILIPPINE WOMEN'S UNIVERSITY</div>
                        <div>Bachelor of Science in Social Work</div>
                        <div>April 2017 - Present</div>
                    </div>

                    <div className = "bulletedContainer">
                        <div>MAPUA UNIVERSITY</div>
                        <div>Bachelor of Science in Technical Communication</div>
                        <div>July 2013 - April 2016</div>
                    </div>

                    <div className = "bulletedContainer">
                        <div>MAPUA UNIVERSITY</div>
                        <div>Bachelor of Science in Architecture</div>
                        <div>July 2011 - April 2012</div>
                    </div>
                </div>

                <div className = "awardHighlightsContainer">
                    <div className = "highlighted">
                        <strong>AWARDS & CERTIFICATES</strong>
                    </div>

                    <div className = "bulletedContainer">
                        <div>Philippine Journalism Research Conference</div>
                        <div>University of the Philippines – Diliman, Quezon City</div>
                        <div>March 2014</div>
                    </div>

                    <div className = "bulletedContainer">
                        <div>Central Student Council | Logistics Committee</div>
                        <div>Mapua University - Intramuros, Manila</div>
                        <div>SY 2014-2015</div>
                    </div>

                    <div className = "bulletedContainer">
                        <div>Product Specifics Training for AT&T Uverse Green</div>
                        <div>IBEX Global Solutions Inc. – Frontera Dr., Pasig City</div>
                        <div>July 2016</div>
                    </div>

                    <div className = "bulletedContainer">
                        <div>Top Agent for DTV</div>
                        <div>IBEX Global Solutions Inc. – Frontera Dr., Pasig City</div>
                        <div>July 2016</div>
                    </div>
                    
                    <div className = "bulletedContainer">
                        <div>Products Specifics Training for AT&T ISM</div>
                        <div>IBEX Global Solutions Inc. - Shaw, Mandaluyong City</div>
                        <div>August 2016</div>
                    </div>
                    
                    <div className = "bulletedContainer">
                        <div>Products Specifics Training for Paypal Accounts</div>
                        <div>VXI Global Solutions, Inc. Muñoz, Quezon City</div>
                        <div>March 2019</div>
                    </div>

                    <div className = "bulletedContainer">
                        <div>Top Teammate</div>
                        <div>VXI Global Solutions, Inc. Muñoz, Quezon City</div>
                        <div>March 2019</div>
                    </div>
                    
                    <div className = "bulletedContainer">
                        <div>Products Specifics Training for Paypal Transactions</div>
                        <div>VXI Global Solutions, Inc. Muñoz, Quezon City</div>
                        <div>August 2019</div>
                    </div>
                                        
                    <div className = "bulletedContainer">
                        <div>Logistics and Documentations Volunteer</div>
                        <div>Escolta Block Festival 2019</div>
                        <div>November 2019</div>
                    </div>
                </div>           
            </div> */}

        </div>    
        )
    }
}

export default Container;
