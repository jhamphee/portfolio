import React from 'react';
import './App.css';

import About from './Components/About';
import Skills from './Components/Skills';
import Portfolio from './Components/Portfolio';
import Connect from './Components/Connect';

import {BrowserRouter, Route, Link} from 'react-router-dom';
import Fade from 'react-reveal/Fade';


class App extends React.Component{

  links = ["ABOUT", "SKILLS", "PORTFOLIO", "CONNECT"]

  navLinks = this.links.map(link => {
    return <div className = "navLink">
            <a href = {"/" + link.toLowerCase()}>{link}</a>
           </div>
  });


  render(){
    return(
      <BrowserRouter>
        <div className = "App clearfix">
          <div className = "navBar clearfix">

            <div className = "nameLogoContainer">
              <div>JHAMILA</div>
              <div>PADILLA</div>
            </div>
  
            <div className = "navLinkContainer">
              {this.navLinks}
            </div>
          </div>
          
          <div className = "bodyContainer">
          <Fade>
            <Route path = "/about">
              <About/>
            </Route>

            <Route path = "/skills">
              <Skills/>
            </Route>

            <Route path = "/portfolio">
              <Portfolio/>
            </Route>

            <Route path = "/connect">
              <Connect/>
            </Route>
          </Fade>
          </div>
         

          <footer>© 2020 Jham Of All Trades, All Rights Reserved.</footer>
        </div>
      </BrowserRouter>
    )
  }
}

export default App;
